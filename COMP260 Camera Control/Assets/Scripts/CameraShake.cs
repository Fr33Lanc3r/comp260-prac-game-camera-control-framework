﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public float amplitude = 0.1f;
    public float frequency = 10f;
    public float duration = 0.5f;

    private float timer = 0f;
    private Vector3 direction = Vector3.zero;

	void Start () {
		
	}
	
	void Update () {
		if (timer > 0) {
            timer -= Time.deltaTime;

            if (timer > 0) {
                //shake the camera
                float t = (duration - timer) * frequency;
                float a = amplitude * Mathf.Sin(t * Mathf.PI * 2);
                transform.localPosition = direction * a;
            } else {
                //reset to zero offset
                transform.localPosition = Vector3.zero;
            }
        }
	}

    public void Shake(Vector3 dir) {
        //reset the timer
        timer = duration;
        direction = dir.normalized;
    }
}
